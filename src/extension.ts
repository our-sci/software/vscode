"use strict";
// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from "vscode";
import axios from "axios";
import * as path from "path";
import * as fs from "fs";
import * as _ from "lodash";
import * as papaparse from "papaparse";

interface Survey {
  formTitle: string;
  date: string;
  formId: string;
}

interface SurveyItem extends vscode.QuickPickItem {
  name: string;
}

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export async function activate(context: vscode.ExtensionContext) {
  // Use the console to output diagnostic information (console.log) and errors (console.error)
  // This line of code will only be executed once when your extension is activated
  console.log('Congratulations, your extension "oursci" is now active!');

  // The command has been defined in the package.json file
  // Now provide the implementation of the command with  registerCommand
  // The commandId parameter must match the command field in package.json
  let disposable = vscode.commands.registerCommand(
    "extension.selectSurvey",
    async () => {
      const surveys = await axios("https://app.our-sci.net/api/survey/list");
      console.log(surveys.data.length);
      const items: SurveyItem[] = surveys.data.map((s: Survey) => {
        const infos = {
          label: s.formTitle,
          description: s.formId
        };

        const parts = s.formTitle.split(";");
        if (parts.length === 2) {
          infos.label = parts[0];
        }

        return {
          label: infos.label,
          description: infos.description,
          currentVersion: s.date,
          name: s.formId
        };
      });

      const item = await vscode.window.showQuickPick(items, {
        placeHolder: "Pick a survey",
        matchOnDescription: true
      });
      if (!item) {
        return;
      }

      console.log(+" <= workspace folder");
      vscode.window.showInformationMessage("Selected " + item.label);

      const workspaceFolders = _.get(vscode, "workspace.workspaceFolders");
      if (!workspaceFolders || workspaceFolders.length <= 0) {
        return;
      }

      const localFolder = path.join(
        workspaceFolders[0].uri.fsPath,
        ".oursci-surveys"
      );

      if (!fs.existsSync(localFolder)) {
        fs.mkdirSync(localFolder);
      }

      const csv = await axios(
        "https://app.our-sci.net/api/survey/result/csv/by-form-id/" +
          (item as SurveyItem).name +
          "?groupDelimiter=/"
      );

      /**
      const autoGeneratedId = item.name
        .replace(/[^A-Za-z0-9_]/g, "_")
        .toLowerCase();
      const surveyVar = item.label.replace(/[^A-Za-z0-9_]/g, "_").toLowerCase();
      **/

      const csvData = csv.data;
      fs.writeFileSync(path.join(localFolder, "survey.csv"), csvData);
      const parsed = papaparse.parse(csvData);
      const surveyObject: string = parsed.data[0]
        .map((k: string) => `'${k}' : app.getAnswer('${k}')`)
        .join(",\n");
      const args = parsed.data[0].map((d: string) => `'${d}'`).join("|");
      let typeScript = `
import { app } from "@oursci/scripts";

let err = null;

/**
 * calls previously passed onAnswerMissing function if question has not been answered
 * @param {(${args})} answer
 */
export function req(question) {
  if (!app.getAnswer(question)) {
    err(question);
    return false;
  }
  return true;
}


/**
 * returns false if questions is not answered with eq
 * @param {(${args})} question
 * @param {string} eq the case insensitive string to compare to
 */
export function sel(question, eq = "yes") {
  const answer = app.getAnswer(question);
  if (answer && answer.toLowerCase() === eq.toLowerCase()) {
    return true;
  }
  return false;
}

/**
 *
 * @param {function} onAnswerMissing This is called when an answer is missing, as argument the
 * question ID is given
 */
export const survey = (onAnswerMissing) => {
  err = onAnswerMissing;
  return {\n${surveyObject}}
};
`;

      fs.writeFileSync(path.join(localFolder, "survey.js"), typeScript);
    }
  );

  context.subscriptions.push(disposable);
}

// this method is called when your extension is deactivated
export function deactivate() {}

/**
 *  fs.writeFileSync(
        path.join(localFolder, autoGeneratedId + ".csv"),
        csvData
      );
      const parsed = papaparse.parse(csvData);
      let typeScript = "namespace " + surveyVar + " {\n";

      typeScript += "export enum Questions {\n";

      _.forEach(parsed.data[0], (k, idx) => {
        typeScript += '"' + k + "\",\n";
      });
      typeScript += "};\n";

      typeScript += "export interface Survey {\n";
      _.forEach(parsed.data[0], (k, idx) => {
        try {
          const defaultValue = parsed.data[1][idx];
          typeScript += '"' + k + "\": '" + defaultValue + "';\n";
        } catch (error) {
          typeScript += '"' + k + "\": '';\n";
        }
      });
      typeScript += "};\n";
      typeScript += "};\n";

      fs.writeFileSync(
        path.join(localFolder, autoGeneratedId + ".ts"),
        typeScript
      );
 */
