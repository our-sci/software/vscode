import { app } from "@oursci/scripts";

let err = null;

/**
 *
 * @param {function} onAnswerMissing This is called when an answer is missing, as argument the
 * question ID is given
 */
export function init(onAnswerMissing) {
  err = onAnswerMissing();
}

/**
 *
 * @param {($QUESTIONS)} answer
 */
export function req(question) {
  if (!app.getAnswer(question)) {
    err(question);
    return false;
  }
  return true;
}

/**
 * returns false if questions is not answered with eq
 * @param {($QUESTIONS)} question
 * @param {string} eq the case insensitive string to compare to
 */
function sel(question, eq = "yes") {
  if (survey[name] && survey[name].toLowerCase() === eq.toLowerCase()) {
    return true;
  }
  return false;
}

export const survey = $SURVEY;
